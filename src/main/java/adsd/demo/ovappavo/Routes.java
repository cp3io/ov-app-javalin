package adsd.demo.ovappavo;

import gg.jte.ContentType;
import gg.jte.TemplateEngine;
import gg.jte.resolve.ResourceCodeResolver;
import io.javalin.Javalin;
import io.javalin.rendering.template.JavalinJte;

public class Routes {

    private final AppController appController = new AppController();

    public Routes() {
        var app = startApp();
        createRoutes(app);
    }

    private Javalin startApp() {
        ResourceCodeResolver codeResolver = new ResourceCodeResolver("templates");
        var templateEngine = TemplateEngine.create(codeResolver, ContentType.Html);
        JavalinJte.init(templateEngine);
        return Javalin.create().start(7070);
    }

    private void createRoutes(Javalin app) {
        app.get("/", appController::renderMain);
        app.get("/search", appController::renderSearch);
    }
}
