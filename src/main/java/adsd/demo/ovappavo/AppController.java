package adsd.demo.ovappavo;

import java.util.HashMap;
import io.javalin.http.Context;

public class AppController {

    private final Data data = Data.getInstance();

    public void renderMain(Context ctx) {
        HashMap<String, String[]> params = new HashMap<>();
        params.put("locations", data.getLocations());
        params.put("transport_types", data.getTransportTypes());
        ctx.render("main.jte", params);
    }

    public void renderSearch(Context ctx) {
        HashMap<String, String> params = new HashMap<>();
        params.put("from", ctx.queryParam("from"));
        params.put("to", ctx.queryParam("to"));
        params.put("transport_type", ctx.queryParam("transport_type"));
        ctx.render("search.jte", params);
    }
}
