package adsd.demo.ovappavo;

public class Data {
    private static Data INSTANCE;

    private Data() {
    }

    public static Data getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Data();
        }
        return INSTANCE;
    }

    private final String[] transportTypes = { "bus", "tram", "train", "plane" };
    private final String[] locations = {
            "Amsterdam",
            "Amersfoort",
            "Arnhem",
            "Nijmegen",
            "Utrecht",
            "Rotterdam",
            "Vlissingen",
            "Maastricht",
            "Groningen"
    };

    public String[] getTransportTypes() {
        return transportTypes;
    }

    public String[] getLocations() {
        return locations;
    }
}
